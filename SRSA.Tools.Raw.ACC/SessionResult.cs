﻿namespace SRSA.Tools.Raw.ACC;

using System.Collections.Generic;
using Newtonsoft.Json;

internal class SessionResult
{
    [JsonProperty("bestlap")]
    public int Bestlap { get; set; }

    [JsonProperty("bestSplits")]
    public List<int> BestSplits { get; set; }

    [JsonProperty("isWetSession")]
    public int IsWetSession { get; set; }

    [JsonProperty("type")]
    public int Type { get; set; }

    [JsonProperty("leaderBoardLines")]
    public List<LeaderBoardLine> LeaderBoardLines { get; set; }
}