﻿namespace SRSA.Tools.Raw.ACC;

using System.Linq;
using Tools.Raw.Common;
using Tools.Results.Domain;
using System.Collections.Generic;
using Newtonsoft.Json;

public class AccResult: IParsedResult
{
    [JsonProperty("sessionType")]
    internal string SessionType { get; set; }

    [JsonProperty("trackName")]
    public string TrackName { get; internal set; }

    [JsonProperty("sessionIndex")]
    internal int SessionIndex { get; set; }

    [JsonProperty("raceWeekendIndex")]
    internal int RaceWeekendIndex { get; set; }

    [JsonProperty("sessionResult")]
    internal SessionResult SessionResult { get; set; }

    [JsonProperty("laps")]
    internal List<Lap> Laps { get; set; }

    [JsonProperty("penalties")]
    internal List<Penalty> Penalties { get; set; }


    public IEnumerable<IDriverSession> CreateDriverSessions(string session) => SessionResult.LeaderBoardLines
        .Select(x => new DriverSession(session, x));
}