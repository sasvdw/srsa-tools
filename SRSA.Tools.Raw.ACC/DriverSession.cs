﻿namespace SRSA.Tools.Raw.ACC;

using System.Collections.Generic;
using Tools.Results.Domain;
internal class DriverSession(string session, LeaderBoardLine leaderBoardLine) : IDriverSession
{
    public string Id => leaderBoardLine.CurrentDriver.PlayerId;
    public string Name => $"{leaderBoardLine.CurrentDriver.FirstName} {leaderBoardLine.CurrentDriver.LastName}";
    public uint BestTime => leaderBoardLine.Timing.BestLap;
    public string Session { get; } = session;
    public string Car => Definitions.Cars.Models.GetValueOrDefault(leaderBoardLine.Car.CarModel, $"Car Model #{leaderBoardLine.Car.CarModel}");
    public uint TotalLaps => leaderBoardLine.Timing.LapCount;

    
}