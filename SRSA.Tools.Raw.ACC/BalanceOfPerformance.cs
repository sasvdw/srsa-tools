﻿namespace SRSA.Tools.Raw.ACC;

using System.Collections.Generic;
using Newtonsoft.Json;

public class BalanceOfPerformance
{
    [JsonProperty("entries")]
    public List<Entry> Entries { get; set; } = [];
    public class Entry
    {
        [JsonProperty("track")]
        public string Track { get; set; }
        
        [JsonProperty("carModel")]
        public int CarModel { get; set; }
        
        [JsonProperty("ballastKg")]
        public int BallastKg { get; set; }
        
        [JsonProperty("restrictor")]
        public int Restrictor { get; set; }
    }
}