using Moq;
using NUnit.Framework;

namespace SRSA.Tools.Results.Domain.Tests.TrackResults
{
    [SetUpFixture]
    public class Spec
    {
        private static string trackName;
        // private static (string id, string name, uint bestTime, string session, string car, uint totalLaps) driverOneSessionOne;
        // private static (string id, string name, uint bestTime, string session, string car, uint totalLaps) driverTwoSessionOne;

        private static Mock<IDriverSession> driverOneSessionOne;
        private static Tools.Results.Domain.Driver driverOne;
        private static Mock<IDriverSession> driverTwoSessionOne;
        private static Tools.Results.Domain.Driver driverTwo;

        private static Mock<IDriverSession> driverOneSessionTwo;
        private static Mock<IDriverSession> driverThreeSessionTwo;
        private static Tools.Results.Domain.Driver driverThree;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            trackName = "Test Track";
            // driverOneSessionOne = ("Id 1", "Driver 1", 111, "Test Session 1", "Test Car 1", 10);
            // driverTwoSessionOne = ("Id 2", "Driver 2", 112, "Test Session 1", "Test Car 2", 11);

            driverOneSessionOne = new Mock<IDriverSession>();
            driverOneSessionOne.Setup(x => x.Id).Returns("Id 1");
            driverOneSessionOne.Setup(x => x.Name).Returns("Driver 1");
            driverOneSessionOne.Setup(x => x.BestTime).Returns(111);
            driverOneSessionOne.Setup(x => x.Session).Returns("Test Session 1");
            driverOneSessionOne.Setup(x => x.Car).Returns("Test Car 1");
            driverOneSessionOne.Setup(x => x.TotalLaps).Returns(10);

            driverOne = driverOneSessionOne.Object.Create();
            driverOneSessionOne.Setup(x => x.Create()).Returns(driverOne);

            driverTwoSessionOne = new Mock<IDriverSession>();
            driverTwoSessionOne.Setup(x => x.Id).Returns("Id 2");
            driverTwoSessionOne.Setup(x => x.Name).Returns("Driver 2");
            driverTwoSessionOne.Setup(x => x.BestTime).Returns(112);
            driverTwoSessionOne.Setup(x => x.Session).Returns("Test Session 1");
            driverTwoSessionOne.Setup(x => x.Car).Returns("Test Car 2");
            driverTwoSessionOne.Setup(x => x.TotalLaps).Returns(11);

            driverTwo = driverTwoSessionOne.Object.Create();
            driverTwoSessionOne.Setup(x => x.Create()).Returns(driverTwo);

            driverOneSessionTwo = new Mock<IDriverSession>();
            driverOneSessionTwo.Setup(x => x.Id).Returns("Id 1");
            driverOneSessionTwo.Setup(x => x.Name).Returns("Driver 1");
            driverOneSessionTwo.Setup(x => x.BestTime).Returns(111);
            driverOneSessionTwo.Setup(x => x.Session).Returns("Test Session 2");
            driverOneSessionTwo.Setup(x => x.Car).Returns("Test Car 1");
            driverOneSessionTwo.Setup(x => x.TotalLaps).Returns(10);

            driverThreeSessionTwo = new Mock<IDriverSession>();
            driverThreeSessionTwo.Setup(x => x.Id).Returns("Id 3");
            driverThreeSessionTwo.Setup(x => x.Name).Returns("Driver 3");
            driverThreeSessionTwo.Setup(x => x.BestTime).Returns(112);
            driverThreeSessionTwo.Setup(x => x.Session).Returns("Test Session 2");
            driverThreeSessionTwo.Setup(x => x.Car).Returns("Test Car 3");
            driverThreeSessionTwo.Setup(x => x.TotalLaps).Returns(11);

            driverThree = driverThreeSessionTwo.Object.Create();
            driverThreeSessionTwo.Setup(x => x.Create()).Returns(driverThree);
        }

        #region Nested type: Given_empty_track_results_When_merging_driver_sessions

        [TestFixture]
        public class Given_empty_track_results_When_merging_driver_sessions
        {
            private Tools.Results.Domain.TrackResults trackResults;
            private IDriverSession[] driverSessions;

            [SetUp]
            public void Setup()
            {
                this.trackResults = new Tools.Results.Domain.TrackResults(trackName);

                this.driverSessions = new[] { driverOneSessionOne.Object, driverTwoSessionOne.Object };

                this.trackResults.Merge(this.driverSessions);
            }

            [TearDown]
            public void Teardown()
            {
                driverOneSessionOne.Invocations.Clear();
                driverTwoSessionOne.Invocations.Clear();
            }

            [Test]
            public void Then_driver_list_contains_same_amount_of_entries_as_merged_driver_sessions()
            {
                Assert.AreEqual(this.driverSessions.Length, this.trackResults.Drivers.Count);
            }

            [Test]
            public void Then_drivers_are_contained_in_track_results()
            {
                CollectionAssert.Contains(this.trackResults.Drivers.Values, driverOne);
                CollectionAssert.Contains(this.trackResults.Drivers.Values, driverTwo);
            }
        }

        #endregion

        #region Nested type: Given_track_name_When_creating_track_result_instance

        [TestFixture]
        public class Given_track_name_When_creating_track_result_instance
        {
            private Tools.Results.Domain.TrackResults trackResults;

            [SetUp]
            public void Setup()
            {
                this.trackResults = new Tools.Results.Domain.TrackResults(trackName);
            }

            [Test]
            public void Then_drivers_list_is_empty()
            {
                Assert.IsEmpty(this.trackResults.Drivers);
            }

            [Test]
            public void Then_track_name_is_set()
            {
                Assert.AreEqual(trackName, this.trackResults.TrackName);
            }
        }

        #endregion

        #region Nested type: Given_track_results_with_driver_sessions_added_When_merging_more_driver_sessions

        [TestFixture]
        public class Given_track_results_with_driver_sessions_added_When_merging_more_driver_sessions
        {
            private Tools.Results.Domain.TrackResults trackResults;

            [SetUp]
            public void Setup()
            {
                this.trackResults = new Tools.Results.Domain.TrackResults(trackName);
                var driverSessions = new[] { driverOneSessionOne.Object, driverTwoSessionOne.Object };
                this.trackResults.Merge(driverSessions);

                var moreDriverSessions = new[] { driverOneSessionTwo.Object, driverThreeSessionTwo.Object };
                this.trackResults.Merge(moreDriverSessions);
            }

            [TearDown]
            public void Teardown()
            {
                driverOneSessionOne.Invocations.Clear();
                driverTwoSessionOne.Invocations.Clear();
                driverOneSessionTwo.Invocations.Clear();
                driverThreeSessionTwo.Invocations.Clear();
            }

            [Test]
            public void Then_contains_new_driver()
            {
                CollectionAssert.Contains(this.trackResults.Drivers.Values, driverThree);
            }

            [Test]
            public void Then_existing_driver_is_merged_with_latest_session()
            {
                driverOneSessionTwo.Verify(x => x.Merge(driverOne));
            }
        }

        #endregion
    }
}
