﻿namespace SRSA.Tools.Results;

using System;
using System.CommandLine;
using System.IO;
using System.Text;
using Domain;
using Raw.AC;
using Raw.ACC;
using Raw.Common;
public static class ResultsHost
{
    private static readonly Argument<DirectoryInfo> InputDirectoryArgument = new Argument<DirectoryInfo>(
        "The directory containing the result files to process."
    ).ExistingOnly();
    
    private static readonly Option<DirectoryInfo> OutputDirectoryOption = new Option<DirectoryInfo>(
        new[] {"--outputDirectory", "-outputDir"},
        () => new DirectoryInfo(Environment.CurrentDirectory),
        "The directory containing where the processed CSV results will be stored."
    ).ExistingOnly();
    
    private static readonly Option<bool> OutputDriverIdsOption = new(
        new []{ "--outputDriverIds", "-outputIds" },
        "Outputs Driver Ids as captured by the sim."
    );

    public static Command BuildCommand()
    {
        var accCommand = BuildAccCommand();
        var acCommand = BuildAcCommand();

        var rootCommand = new Command("hlq-results")
        {
            accCommand,
            acCommand,
            InputDirectoryArgument, 
            OutputDirectoryOption,
            OutputDriverIdsOption
        };
        rootCommand.Description = "SRSA HLQ Results processor";

        return rootCommand;
    }

    private static Command BuildAccCommand()
    {
        var accCommand = new Command("ACC")
        {
            Description = "Process ACC Results"
        };

        accCommand.SetHandler(
            ProcessAccResults,
            InputDirectoryArgument,
            OutputDirectoryOption,
            OutputDriverIdsOption
        );

        return accCommand;
    }

    private static Command BuildAcCommand()
    {
        var acCommand = new Command("AC")
        {
            Description = "Process AC Results"
        };


        acCommand.SetHandler(
            ProcessAcResults,
            InputDirectoryArgument,
            OutputDirectoryOption,
            OutputDriverIdsOption
        );

        return acCommand;
    }

    private static async Task ProcessAccResults(DirectoryInfo inputDirectory, DirectoryInfo outputDirectory, bool outputDriverIds)
    {
        var fileParser = new FileParser<AccResult>(Encoding.Unicode);
        var accResultsRepository = new TrackResultsRepository<AccResult>(fileParser, inputDirectory);
        await Process(accResultsRepository, outputDirectory, outputDriverIds);
    }

    private static async Task ProcessAcResults(DirectoryInfo inputDirectory, DirectoryInfo outputDirectory, bool outputDriverIds)
    {
        var fileParser = new FileParser<AcResult>(Encoding.UTF8);
        var accResultsRepository = new TrackResultsRepository<AcResult>(fileParser, inputDirectory);
        await Process(accResultsRepository, outputDirectory, outputDriverIds);
    }
    
    
    
    private static async Task Process(ITrackResultsRepository repository, DirectoryInfo outputDirectory, bool outputDriverIds)
    {
        var tracks = await repository.GetAllAsync();

        foreach(var track in tracks)
        {
            Console.WriteLine($"Writing results for track: {track.Key}");
            using var writer = new CSVWriter(outputDirectory, track.Key);

            var drivers = track.Value.Drivers.Values;
            var lines = track.Value.Drivers.OrderBy(x => x.Value.BestTime)
                .Zip(Enumerable.Range(1, drivers.Count), (driver, position) => (driver, position))
                .Select((line) => new ResultLine
                {
                    Id = line.driver.Key,
                    Position = (uint)line.position,
                    Driver = line.driver.Value.Name,
                    BestTime = TimeSpan.FromMilliseconds(line.driver.Value.BestTime),
                    FromSession = line.driver.Value.Session,
                    UsingCar = line.driver.Value.Car,
                    TotalLapsOverAllSessions = line.driver.Value.TotalLaps
                });

            await writer.WriteResults(lines, outputDriverIds);
        }
    }
}