﻿namespace SRSA.Tools.Results.Domain;

public interface IDriverSession
{
    public string Id { get; }
    public string Name { get; }
    public uint BestTime { get; }
    public string Session { get; }
    public string Car { get; }
    public uint TotalLaps { get; }

    public Driver Create() => new(Name, BestTime, Session, Car, TotalLaps);

    public void Merge(Driver driver) => driver.Merge(BestTime, Session, Car, TotalLaps);
}