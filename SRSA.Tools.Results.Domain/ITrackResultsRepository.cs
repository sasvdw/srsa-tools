﻿namespace SRSA.Tools.Results.Domain;

using System.Collections.Generic;
using System.Threading.Tasks;

public interface ITrackResultsRepository
{
    public Task<IDictionary<string, TrackResults>> GetAllAsync();
}