﻿namespace SRSA.Tools.Results.Domain;

using System.Collections.Concurrent;
using System.Collections.Generic;

public class TrackResults
{
    private readonly ConcurrentDictionary<string, Driver> drivers;
    public string TrackName { get; }

    public IDictionary<string, Driver> Drivers => this.drivers;

    public TrackResults(string trackName)
    {
        this.TrackName = trackName;
        this.drivers = new ConcurrentDictionary<string, Driver>();
    }

    public void Merge(IEnumerable<IDriverSession> driverSessions)
    {
        foreach(var driverSession in driverSessions)
        {
            this.drivers.AddOrUpdate(
                driverSession.Id,
                _ => driverSession.Create(),
                (_, driver) =>
                {
                    driverSession.Merge(driver);
                    return driver;
                }
            );
        }
    }
}