﻿namespace SRSA.Tools.Integration.accweb;

using System.Text.Json;
using Flurl.Http;
using Flurl.Http.Configuration;
using Microsoft.Kiota.Abstractions.Authentication;

public class AccwebAccessTokenProvider : IAccessTokenProvider
{
    private readonly string password;
    private readonly IFlurlClient flurlClient;
    
    private LoginResponse? loginResponse;

    public AccwebAccessTokenProvider(Uri baseUrl, string password)
    {
        this.password = password;
        flurlClient = new FlurlClient(baseUrl.ToString()).WithSettings(s =>
        {
            s.JsonSerializer = new DefaultJsonSerializer(new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.SnakeCaseLower
            });
        });
        this.AllowedHostsValidator = new AllowedHostsValidator([baseUrl.Host]);
    }
    
    public async Task<string> GetAuthorizationTokenAsync(Uri uri, Dictionary<string, object>? additionalAuthenticationContext = null, CancellationToken cancellationToken = new CancellationToken())
    {
        if (loginResponse == null || loginResponse.Expire >= DateTime.Now)
        {
            var request = flurlClient.Request("login");

            var flurlResponse = await request.PostJsonAsync(new LoginRequest { Password = password }, cancellationToken: cancellationToken);

            loginResponse = await flurlResponse.GetJsonAsync<LoginResponse>();
        }

        return loginResponse.Token;
    }
    public AllowedHostsValidator AllowedHostsValidator
    {
        get;
    }

    internal class LoginRequest
    {
        public required string Password { get; init; }
    }

    internal class LoginResponse
    {
        public bool Admin { get; set; }
        public int Code { get; set; }
        public DateTime Expire { get; set; }
        public bool Mod { get; set; }
        public bool ReadOnly { get; set; }
        public string Token { get; set; }
        public string UserName { get; set; }
    }
}