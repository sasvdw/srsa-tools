﻿namespace SRSA.Tools.Integration.SimRacingGP.Models.Query.BalanceOfPerformance;

public class AccBalanceOfPerformance
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string? TrackId { get; set; }
    public DateTime LastUpdatedTimestamp { get; set; }
}