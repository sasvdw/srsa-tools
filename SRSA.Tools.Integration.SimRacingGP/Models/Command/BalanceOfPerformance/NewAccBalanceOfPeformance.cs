﻿namespace SRSA.Tools.Integration.SimRacingGP.Models.Command.BalanceOfPerformance;

public class NewAccBalanceOfPeformance
{
    public Dictionary<int, Entry> Bops { get; set; }
    public string Game { get; set; }
    public string LeagueId { get; set; }
    public string Name { get; set; }
    public string? TrackId { get; set; }

    public class Entry
    {
        public int Ballastkg { get; set; }
        public int Restrictor { get; set; }
    }
}