﻿namespace SRSA.Tools.Raw.AC;

using System.Collections.Generic;
using Newtonsoft.Json;

internal class Lap
{
    [JsonProperty("DriverName")]
    public string DriverName { get; set; }

    [JsonProperty("DriverGuid")]
    public string DriverGuid { get; set; }

    [JsonProperty("CarId")]
    public int CarId { get; set; }

    [JsonProperty("CarModel")]
    public string CarModel { get; set; }

    [JsonProperty("Timestamp")]
    public int Timestamp { get; set; }

    [JsonProperty("LapTime")]
    public int LapTime { get; set; }

    [JsonProperty("Sectors")]
    public List<int> Sectors { get; set; }

    [JsonProperty("Cuts")]
    public int Cuts { get; set; }

    [JsonProperty("BallastKG")]
    public int BallastKG { get; set; }

    [JsonProperty("Tyre")]
    public string Tyre { get; set; }

    [JsonProperty("Restrictor")]
    public int Restrictor { get; set; }
}