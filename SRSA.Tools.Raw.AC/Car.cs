﻿namespace SRSA.Tools.Raw.AC;

using Newtonsoft.Json;

internal class Car
{
    [JsonProperty("CarId")]
    public int CarId { get; set; }

    [JsonProperty("Driver")]
    public Driver Driver { get; set; }

    [JsonProperty("Model")]
    public string Model { get; set; }

    [JsonProperty("Skin")]
    public string Skin { get; set; }

    [JsonProperty("BallastKG")]
    public int BallastKG { get; set; }

    [JsonProperty("Restrictor")]
    public int Restrictor { get; set; }
}