﻿namespace SRSA.Tools.Raw.AC;

using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using Tools.Raw.Common;
using Tools.Results.Domain;
public class AcResult: IParsedResult
{
    [JsonProperty("TrackName")]
    public string TrackName { get; set; }

    [JsonProperty("TrackConfig")]
    internal string TrackConfig { get; set; }

    [JsonProperty("Type")]
    internal string Type { get; set; }

    [JsonProperty("DurationSecs")]
    internal int DurationSecs { get; set; }

    [JsonProperty("RaceLaps")]
    internal int RaceLaps { get; set; }

    [JsonProperty("Cars")]
    internal List<Car> Cars { get; set; }

    [JsonProperty("Result")]
    internal List<Result> Result { get; set; }

    [JsonProperty("Laps")]
    internal List<Lap> Laps { get; set; }

    [JsonProperty("Events")]
    internal object Events { get; set; }

    public IEnumerable<IDriverSession> CreateDriverSessions(string session) => Result.Select(
        result =>
        {
            uint totalLaps = (uint)Laps.Count(lap => lap.DriverGuid == result.DriverGuid);

            return new DriverSession(session, result, totalLaps);
        }
    );
}