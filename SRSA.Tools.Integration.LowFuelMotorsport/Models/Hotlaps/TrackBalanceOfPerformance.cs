﻿namespace SRSA.Tools.Integration.LowFuelMotorsport.Models.Hotlaps;

public class TrackBalanceOfPerformance
{
    public string TrackName { get; set; }
    public Dictionary<string, Entry[]> Bop { get; set; }
    public int BopVersion { get; set; }
    public string ActiveSince { get; set; }
 
    public class Entry
    {
        public int CarModel { get; set; }
        public string CarName { get; set; }
        public int Caryear { get; set; }
        public int Ballast { get; set; }
        public int BallastChange { get; set; }
    }
}