# SRSA Tools

[Sim Race South Africa](https://simracesa.co.za/) uses what's known as Hot Lap Qualifier (HLQ for short) servers to determin splits (divisions) for a given race week. Racers have a reserved slot on one of multiple servers running from Thursday to Sunday. These servers are configured to cycle every 2 hours to keep conditions relatively constant. Once HLQ is completed, the resulting `json` result files need to be interrogated for the best time of each driver and these results then get aggregated together into a single sorted list of drivers sorted from fastest to slowest.

This application serves to ease the amount of work an administrator has to do. All result files can simply be put in a single directory and the application can be run to spit out a CSV containing the sorted results. Division splits can then be broken down accordingly, but that is still a manual process from an adminstration point of view.

# Basic app usage

```
SRSAHLQResults
  SRSA HLQ Results processor

Usage:
  SRSAHLQResults [options] [command]

Options:
  -inputDir, --inputDirectory <inputDirectory>     The directory containing the result files to process. [default: D:\Sim Racing\SRSA\Results Processing\ACC Results Processing]
  -outputDir, --outputDirectory <outputDirectory>  The directory containing where the processed CSV results will be stored. [default: D:\Sim Racing\SRSA\Results Processing\ACC Results Processing]
  -outputIds, --outputDriverIds                    Outputs Driver Ids as captured by the sim.
  --version                                        Show version information
  -?, -h, --help                                   Show help and usage information

Commands:
  ACC  Process ACC Results
  AC   Process AC Results
```

# Installation

This is a commandline application. It can either be run from a shell by typing out the commands as listed above, or a shortcut can be created that provide the relevant parameters. The latter is a simpler way to use and will be described first.

## Simple Installation

1. Extract the latest version of the application found on the [Release page](https://gitlab.com/sasvdw/srsa-hlq-results/-/releases) of this repository to a directory of your choosing. 
2. Open the `SRSAHLQResults` directory and locate the `SRSAHLQResults.exe` file. If you have extensions hidden it will just be an application file by the name `SRSAHLQResults`.
3. Right click the `SRSAHLQResults.exe` and choose `Create shortcut` (see image below).

![create-shortcut](Documentation/create-shortcut.png)

4. Right click the newly created shortcut and choose properties, you should be presented with the window below.

![shortcut-properties](Documentation/shortcut-properties.png)

5. Depending on the sim you want to process results for append either "AC" (for Assetto Corsa) or "ACC" (for Assetto Corsa Competizione) to the `Target` field. Keep whatever is in the field as it is the path to the application this shortcut is for.
  * note above that the quotation marks were omitted
6. Grab the directory where your `json` result files are located and put it in the `Start in` field. It's recommended you replace whatever is in the `Start in` field completely while making sure to surround your `json` result files directory with double quotation marks (eg. "D:/Some AC Results")

## Advanced Installation

1. Extract the latest version of the application found on the [Release page](https://gitlab.com/sasvdw/srsa-hlq-results/-/releases) of this repository to a directory of your choosing.
2. Add the directory to the `SRSAHLQResults` directory to your PATH variable.
3. If installed correctly running `SRSAHLQResults -h` from any shell should ouput the same text as is contained above in the Basic app usage section.
