﻿namespace SRSA.Tools.BalanceOfPerformance;

using System.CommandLine.Binding;
using SRSA.Tools.Integration.LowFuelMotorsport;

internal class LowFuelMotorsportApiClientBinder : BinderBase<LowFuelMotorsportApiClient>
{
    protected override LowFuelMotorsportApiClient GetBoundValue(BindingContext bindingContext)
    {
        return new LowFuelMotorsportApiClient();
    }
}