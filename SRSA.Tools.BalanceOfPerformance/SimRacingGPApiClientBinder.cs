﻿namespace SRSA.Tools.BalanceOfPerformance;

using System.CommandLine;
using System.CommandLine.Binding;
using SRSA.Tools.Integration.SimRacingGP;

internal class SimRacingGPApiClientBinder : BinderBase<SimRacingGPApiClient>
{
    private readonly Option<string> sgpTokenOption;
    
    public SimRacingGPApiClientBinder(Option<string> sgpTokenOption)
    {
        this.sgpTokenOption = sgpTokenOption;
    }

    protected override SimRacingGPApiClient GetBoundValue(BindingContext bindingContext)
    {
        var valueForOption = bindingContext.ParseResult.GetValueForOption(sgpTokenOption)!;
        return new SimRacingGPApiClient(valueForOption);
    }
}